# -*- coding: utf-8 -*-
import re
import os
import pytz
from django.db import models
from django.core.mail import send_mail
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.core import validators
from django.conf import settings
from django.templatetags.static import static
from django.core.urlresolvers import reverse
from sorl.thumbnail import get_thumbnail
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, \
    UserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """
    def avatar_filename(instance, filename):
        fname, extension = os.path.splitext(filename)
        fname = instance.username
        return 'user/avatars/%s%s' % (fname, extension)

    username = models.CharField(_('username'), max_length=30, unique=True,
        help_text=_('Required. 30 characters or fewer. Letters, numbers and '
                    '@/./+/-/_ characters'),
        validators=[
            validators.RegexValidator(re.compile('^[\w.@+-]+$'), _('Enter a valid username.'), 'invalid')
        ])
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    avatar = models.ImageField(_('avatar'), null=True, blank=True, upload_to=avatar_filename)
    tz = models.CharField(_('timezone'), null=True, blank=True, max_length=255,
        choices=[(t,t) for t in pytz.common_timezones])

    def ava_tag(self):
        if self.avatar:
            return u'<img src="%s%s" />' % (settings.MEDIA_URL, get_thumbnail(self.avatar, '45x45', crop='center'))
        else:
            return u'<img src="%s" />' % static('img/default_ava.png')
    ava_tag.short_description = 'Ava'
    ava_tag.allow_tags = True

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_absolute_url(self):
        return reverse('profile_view', kwargs={'slug': self.username})

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        if self.first_name or self.last_name:
            full_name = '%s %s' % (self.first_name, self.last_name)
            return full_name.strip()
        else:
            return self.username

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])
