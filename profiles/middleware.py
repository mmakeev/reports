from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone
import pytz


class TimezoneMiddleware(object):
    def process_request(self, request):
        tz = request.session.get('django_timezone')

        if not tz:
            if request.user.is_authenticated():
                tz = request.user.tz or timezone.get_default_timezone()

            if not tz:
                tz = timezone.get_default_timezone()

        if tz:
            timezone.activate(tz)
        else:
            timezone.deactivate()
