from .models import CustomUser


def profiles_vars(request):
    top_users = CustomUser.objects.order_by('date_joined').all()[:20]

    return {
        'top_users': top_users
    }
