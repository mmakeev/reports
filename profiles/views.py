from django.views.generic.edit import UpdateView, FormView
from django.views.generic.detail import DetailView
from django.shortcuts import redirect
from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm
from django.core.urlresolvers import reverse_lazy

from .models import CustomUser


class ProfileView(DetailView):
    model = CustomUser
    template_name = 'profiles/detail.jade'
    slug_field = 'username'
    context_object_name = 'profile'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('login')
        return super(ProfileView, self).dispatch(request, *args, **kwargs)


class EditProfileView(UpdateView):
    model = CustomUser
    fields = ['username', 'first_name', 'last_name', 'email', 'avatar']
    template_name = 'profiles/edit.jade'

    def get_success_url(self):
        return reverse_lazy('profile_edit')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('login')
        return super(EditProfileView, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        messages.success(self.request, 'Profile successfully saved')
        return super(EditProfileView, self).form_valid(form)


class ChangePasswordView(FormView):
    fields = ['password']
    form_class = PasswordChangeForm
    template_name = 'profiles/change_password.jade'

    def get_form(self, form_class):
        return PasswordChangeForm(user=self.request.user, **self.get_form_kwargs())

    def get_success_url(self):
        return reverse_lazy('change_password')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('login')
        return super(ChangePasswordView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        messages.success(self.request, 'Password successfully changed')
        return super(ChangePasswordView, self).form_valid(form)

    def form_invalid(self, form):
        print form.errors
        return super(ChangePasswordView, self).form_invalid(form)
