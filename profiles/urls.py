# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url


from profiles.views import EditProfileView, ChangePasswordView, ProfileView

urlpatterns = patterns('',
    url(r'^login/$', 'django.contrib.auth.views.login',
        {'template_name': 'profiles/auth.jade'}, name='login'
    ),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': 'login'}, name='logout'),
    url(r'^edit/$', EditProfileView.as_view(), name='profile_edit'),
    url(r'^change_password/$', ChangePasswordView.as_view(), name='change_password'),
    url(r'^user/(?P<slug>\w+)/$', ProfileView.as_view(), name='profile_view'),
)
