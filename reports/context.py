from django.conf import settings


def reports_vars(request):
    return {'STATUS_REPORT_LENGTH': settings.STATUS_REPORT_LENGTH}
