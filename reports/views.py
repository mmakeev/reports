from django.views.generic.edit import CreateView, DeleteView
from django.views.generic.list import ListView
from django.shortcuts import redirect
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404

from .models import Report
from .forms import AddReportForm
from profiles.models import CustomUser


class AddReportView(CreateView):
    model = Report
    template_name = 'reports/add.jade'
    form_class = AddReportForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('login')
        return super(AddReportView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('reports_list')

    def form_valid(self, form):
        report = form.save(commit=False)
        report.author = self.request.user
        report.save()
        # messages.success(self.request, 'Thanks for report!')
        return super(AddReportView, self).form_valid(form)


class ListReports(ListView):
    model = Report
    template_name = 'reports/list.jade'
    context_object_name = 'reports'
    paginate_by = 5

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('login')
        return super(ListReports, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListReports, self).get_context_data(**kwargs)
        context['header'] = 'Feed'
        return context


class UserListReports(ListView):
    model = Report
    template_name = 'reports/list.jade'
    context_object_name = 'reports'
    paginate_by = 5

    def get_queryset(self):
        qs = super(UserListReports, self).get_queryset()
        user = get_object_or_404(CustomUser, username=self.kwargs['username'])
        return qs.filter(author=user)

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('login')
        username = kwargs.get('username')
        user = get_object_or_404(CustomUser, username=username)
        return super(UserListReports, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserListReports, self).get_context_data(**kwargs)
        context['header'] = '%s\'s feed' % self.kwargs['username']
        return context


def delete_report(request, pk):
    report = get_object_or_404(Report, pk=pk, author=request.user)
    report.delete()
    messages.success(request, 'Report deleted')

    return redirect(request.META.get('HTTP_REFERER', reverse_lazy('reports_list')))
