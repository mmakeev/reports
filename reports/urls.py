# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url


from .views import AddReportView, ListReports, UserListReports

urlpatterns = patterns('',
    url(r'^add/$', AddReportView.as_view(), name='report_add'),
    url(r'^list/$', ListReports.as_view(), name='reports_list'),
    url(r'^list/(?P<username>\w+)/$', UserListReports.as_view(), name='reports_by_user'),
    url(r'^delete/(?P<pk>\d+)', 'reports.views.delete_report', name='report_delete')
)
