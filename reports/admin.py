from django.contrib import admin

from .models import Report


class ReportAdmin(admin.ModelAdmin):
    list_display = ('status', 'author', 'create',)
    list_filter = ('author', 'create')
admin.site.register(Report, ReportAdmin)
