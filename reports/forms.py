from django import forms
from django.utils.html import strip_tags
from django.conf import settings

from .models import Report


class AddReportForm(forms.ModelForm):
    def clean_status(self):
        return strip_tags(self.cleaned_data['status']).strip()[:settings.STATUS_REPORT_LENGTH]

    class Meta:
        model = Report
        fields = ['status']
