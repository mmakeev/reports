from django.db import models
from django.conf import settings


class Report(models.Model):
    status = models.TextField('status')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                               related_name='feed')
    create = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Report"
        verbose_name_plural = "Reports"
        ordering = ['-create']

    def __unicode__(self):
        return self.status
