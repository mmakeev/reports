// Инициализируем плагины
var gulp = require('gulp'), // Сообственно Gulp JS
    fs = require('fs'),
    jade = require('gulp-jade'), // Плагин для Jade
    path = require('path'),
    merge = require('merge-stream'),
    stylus = require('gulp-stylus'), // Плагин для Stylus
    csso = require('gulp-csso'), // Минификация CSS
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    bower = require('gulp-bower')
    nib = require('nib');

function getFolders(dir) {
    return fs.readdirSync(dir)
      .filter(function(file) {
        return fs.statSync(path.join(dir, file)).isDirectory();
      });
}

// Собираем Stylus
gulp.task('stylus', function() {
    gulp.src('./assets/stylus/style.styl')
        .pipe(stylus({
            use: [nib()]
        })) // собираем stylus
    .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(gulp.dest('../assets/css/')); // записываем css
});

gulp.task('3rdparty', function() {
	gulp.src('./assets/3rdparty/**/*')
		.pipe(gulp.dest('../assets/3rdparty'));
});

// Собираем JS
gulp.task('js', function() {
    gulp.src(['./assets/js/**/*.js', '!./assets/js/3rdparty/**/*.js', '!./assets/js/modules/**/*.js'])
        .pipe(concat('index.js')) // Собираем все JS, кроме 3rdparty и angular
        .pipe(gulp.dest('../assets/js'));

    gulp.src(['./assets/js/modules/**/*.html'])
        .pipe(gulp.dest('../assets/js/modules/'));

    gulp.src(['./assets/js/3rdparty/**/*.js'])
        .pipe(gulp.dest('../assets/js/3rdparty'));
});

gulp.task('angular', function() {
    var folders = getFolders('./assets/js/modules');

    var tasks = folders.map(function(folder) {
        return gulp.src([path.join('./assets/js/modules/', folder, '/app.js'), path.join('./assets/js/modules/', folder, '/*.js')])
            .pipe(concat('app.js'))
            .pipe(gulp.dest('../assets/js/modules/'+folder));
    });

    var jade_tasks = folders.map(function(folder) {
        return gulp.src(path.join('./assets/js/modules/', folder, '/templates/*.jade'))
	        .pipe(jade({
	            pretty: true
	        }))
	        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
            .pipe(gulp.dest('../assets/js/modules/'+folder+'/templates/'));
    });

    return merge(tasks.concat(jade_tasks));
});

// bower libs
gulp.task('bower', function() {
  return bower()
    .pipe(gulp.dest('../assets/vendor/'));
});

// fonts
gulp.task('fonts', function() {
    return gulp.src('./assets/fonts/*')
        .pipe(gulp.dest('../assets/fonts/'));
});

// imgs
gulp.task('images', function() {
    return gulp.src('./assets/img/**/*')
        .pipe(gulp.dest('../assets/img/'));
});

// Запуск сервера разработки gulp watch
gulp.task('default', function() {
    // Предварительная сборка проекта
    gulp.run('stylus');
    gulp.run('js');
    gulp.run('angular');
    gulp.run('bower');
    gulp.run('fonts');
    gulp.run('images');
    gulp.run('3rdparty');

    gulp.watch(['assets/stylus/**/*.styl', 'assets/stylus/blocks/**/*.styl'], function() {
        gulp.run('stylus');
    });
    gulp.watch('assets/js/**/*', function() {
        gulp.run('js');
        gulp.run('angular');
    });
    gulp.watch('assets/fonts/*', function() {
        gulp.run('fonts');
    });
    gulp.watch('assets/img/**/*', function() {
        gulp.run('images');
    });
});

gulp.task('build', function() {
    gulp.run('bower');
    gulp.run('fonts');
    gulp.run('images');

    // css
    gulp.src('./assets/stylus/style.styl')
        .pipe(stylus({
            use: [nib()]
        })) // собираем stylus
    .pipe(csso()) // минимизируем css
    .pipe(gulp.dest('../assets/css/')) // записываем css

    // js
    gulp.src(['./assets/js/**/*.js', '!./assets/js/vendor/**/*.js'])
        .pipe(concat('index.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../assets/js'));

});