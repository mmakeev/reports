from django.shortcuts import redirect


def index(request):
    if request.user.is_authenticated():
        return redirect('report_add')
    else:
        return redirect('login')
