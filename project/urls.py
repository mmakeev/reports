# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r'^profile/', include('profiles.urls')),
    url(r'^reports/', include('reports.urls')),
    url(r'^$', 'project.views.index'),
    url(r'^api/', include('api.urls')),
)

# Django раздает статику только в отладочном режиме
# if settings.DEBUG:
# но запись нужна для работы reverse
urlpatterns += patterns(
    '',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}, name='media'),
)
urlpatterns += staticfiles_urlpatterns()
