from rest_framework import viewsets, generics

from profiles.models import CustomUser
from reports.models import Report
from .serializers import UserSerializer, ReportSerializer


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer


class ReportsViewSet(viewsets.ModelViewSet):
    model = Report
    serializer_class = ReportSerializer


class UserReportsList(generics.ListAPIView):
    model = Report
    serializer_class = ReportSerializer

    def get_queryset(self):
        queryset = super(UserReportsList, self).get_queryset()
        return queryset.filter(author__username=self.kwargs.get('username'))
