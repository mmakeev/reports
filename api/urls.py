from django.conf.urls import patterns, include, url
from rest_framework.routers import DefaultRouter

from .views import UserViewSet, ReportsViewSet, UserReportsList

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'reports', ReportsViewSet)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browseable API.
urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^user_feed/(?P<username>\w+)$', UserReportsList.as_view(), name='user_feed'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
