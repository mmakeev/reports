from rest_framework import serializers

from reports.models import Report
from profiles.models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    feed = serializers.HyperlinkedIdentityField('feed', view_name='user_feed',
                                                lookup_field='username')

    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'email', 'first_name', 'last_name',
                  'date_joined', 'avatar', 'feed')


class ReportSerializer(serializers.ModelSerializer):
    author = serializers.HyperlinkedRelatedField(view_name='customuser-detail')

    class Meta:
        model = Report
        fields = ('id', 'status', 'create', 'author',)
